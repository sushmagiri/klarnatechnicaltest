package com.kll.klarnatechnicaltest.api;

import com.kll.klarnatechnicaltest.models.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface KlarnaWeatherApiInterface {
    //api to post user current location and receive weather details
    @GET("{latitude},{longitude}")
    Call<WeatherResponse> postLocation(@Path("latitude") Double lat, @Path("longitude") Double lng);


}
