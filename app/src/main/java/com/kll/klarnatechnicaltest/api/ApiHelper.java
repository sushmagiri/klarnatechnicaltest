package com.kll.klarnatechnicaltest.api;

import android.content.Context;

import com.kll.klarnatechnicaltest.helper.Utils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiHelper {
    private static final String BASE_URL = "https://api.darksky.net/forecast/2bb07c3bece89caf533ac9a5d23d8417/";

    public KlarnaWeatherApiInterface getApiInterface() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient = addLogging(httpClient);
        httpClient.connectTimeout(100, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiHelper.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
        KlarnaWeatherApiInterface api = retrofit.create(KlarnaWeatherApiInterface.class);

        return api;
    }

    public KlarnaWeatherApiInterface getApiWithCaching(final Context context) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient = addLogging(httpClient);

        httpClient.cache(new Cache(context.getCacheDir(), 10 * 1024 * 1024));
        httpClient.connectTimeout(100,TimeUnit.SECONDS);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                if (Utils.networkIsAvailable(context)) {
                    request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60*10).build();
                } else {
                    request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build();
                }
                return chain.proceed(request);
            }
        });
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiHelper.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
        KlarnaWeatherApiInterface api = retrofit.create(KlarnaWeatherApiInterface.class);

        return api;
    }

    private OkHttpClient.Builder addLogging(OkHttpClient.Builder builder){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        builder.addInterceptor(logging);

        return builder;
    }
}
