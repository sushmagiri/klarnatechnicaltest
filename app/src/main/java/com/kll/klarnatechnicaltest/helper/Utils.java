package com.kll.klarnatechnicaltest.helper;

import android.content.Context;

import java.io.IOException;

public class Utils {
    public static boolean networkIsAvailable(Context c) {
        boolean value = isInternetAvailable();
        return value;
    }

    private static boolean isInternetAvailable() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;

    }
}
