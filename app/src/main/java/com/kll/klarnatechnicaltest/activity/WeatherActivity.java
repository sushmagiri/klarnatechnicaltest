package com.kll.klarnatechnicaltest.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.location.Location;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.kll.klarnatechnicaltest.R;
import com.kll.klarnatechnicaltest.api.ApiHelper;
import com.kll.klarnatechnicaltest.api.KlarnaWeatherApiInterface;
import com.kll.klarnatechnicaltest.helper.Utils;
import com.kll.klarnatechnicaltest.models.WeatherResponse;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherActivity extends AppCompatActivity {

    private double lat, lng;
    private FusedLocationProviderClient fusedLocationClient;
    WeatherResponse weatherResponse;
    private TextView tvCurrentTemp, tvCurrentSummary, tvCurrentPressure,tvCurrentTime,tvCloudeCover,tvVisibility,tvWindGust,tvHumidity;
    private TextView tvDailySummary,tvHourlySummary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        tvCurrentTemp = (TextView) findViewById(R.id.temp);
        tvCurrentSummary=(TextView)findViewById(R.id.current_summary);
        tvCurrentTime=(TextView)findViewById(R.id.time);
        tvCloudeCover=(TextView)findViewById(R.id.cloud_cover);
        tvVisibility=(TextView)findViewById(R.id.visibility);
        tvCurrentPressure=(TextView)findViewById(R.id.pressure);
        tvHumidity=(TextView)findViewById(R.id.humidity);
        tvWindGust=(TextView)findViewById(R.id.wind_gust);
        tvHourlySummary=(TextView)findViewById(R.id.hourly_summary);
        tvDailySummary=(TextView)findViewById(R.id.daily_summary);

        requestPermissionLocation();
    }


    public void getWeatherDetails() {
        final KlarnaWeatherApiInterface api = new ApiHelper().getApiInterface();
        Call<WeatherResponse> call = api.postLocation(lat, lng);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                weatherResponse = response.body();

                tvWindGust.setText(String.valueOf(weatherResponse != null ? weatherResponse.getCurrently().getWindGust() : 0));
                tvCurrentPressure.setText(String.valueOf(weatherResponse != null ? weatherResponse.getCurrently().getPressure() : 0));
                tvHumidity.setText(String.valueOf(weatherResponse != null ? weatherResponse.getCurrently().getHumidity() : 0));
                tvHourlySummary.setText(weatherResponse != null ? weatherResponse.getHourly().getSummary() : null);
                tvDailySummary.setText(weatherResponse != null ? weatherResponse.getDaily().getSummary() : null);

                tvVisibility.setText(String.format("Visibility: %s", String.valueOf(weatherResponse != null ? weatherResponse.getCurrently().getVisibility() : 0)));
                tvCloudeCover.setText(String.format("Cloud Cover: %s", String.valueOf(weatherResponse != null ? weatherResponse.getCurrently().getCloudCover() : 0)));
                tvCurrentTime.setText(String.format("Date: %s", String.valueOf(weatherResponse != null ? weatherResponse.getCurrently().getTime() : 0)));
                tvCurrentSummary.setText(weatherResponse != null ? weatherResponse.getCurrently().getSummary() : null);
                tvCurrentTemp.setText(String.format("%sF", String.valueOf(weatherResponse != null ? weatherResponse.getCurrently().getTemperature() : 0)));
            }


            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    @AfterPermissionGranted(1)
    public void requestPermissionLocation() {

        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(this, perms)) {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                lat = location.getLatitude();
                                lng = location.getLongitude();
                                if (Utils.networkIsAvailable(getApplicationContext())) {

                                    getWeatherDetails();

                                } else {
                                    Toast.makeText(getApplicationContext(), "Please connect to internet and try again.", Toast.LENGTH_LONG).show();
                                }

                            }
                        }
                    });

        } else {
            EasyPermissions.requestPermissions(this, "App needs location permission to show weather", 1, perms);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


}

